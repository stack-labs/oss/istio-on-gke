resource "docker_image" "front" {
  name         = "stacklabs/istio-on-gke-front"
  keep_locally = true
}

resource "docker_container" "front" {
  image = docker_image.front.latest
  name  = "front"
  links = toset([
    docker_container.middleware.name
  ])
  env = [ "FRONT_MIDDLEWARE_URI=http://middleware:8080" ]
  ports {
    internal = 8080
    external = 8080
  }
}