package com.stacklabs.workshop.istioongke.database

import org.slf4j.LoggerFactory
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.ServerResponse.ok
import org.springframework.web.reactive.function.server.router
import reactor.core.publisher.Mono
import reactor.core.publisher.toMono
import java.time.Duration.ofMillis
import java.time.ZonedDateTime
import java.time.ZonedDateTime.now

@Configuration
@EnableConfigurationProperties(DatabaseProperties::class)
@Import(DatabaseHandler::class)
class ApplicationConfiguration {
    @Bean
    fun routes(database: DatabaseHandler) = router {
        GET("/", database::serve)
    }
}

class DatabaseHandler(
        prop: DatabaseProperties
) {

    private val log = LoggerFactory.getLogger(DatabaseHandler::class.java)

    private val name = prop.name
    private val version: String = prop.version
    private val requestWaitingRange = (0..prop.maxLatency).map(Int::toLong)

    val errorProbability = 0..100
    val errorRate = prop.errorRate

    @SuppressWarnings("")
    fun serve(@Suppress("UNUSED_PARAMETER") r: ServerRequest): Mono<ServerResponse> {

        val duration = ofMillis(requestWaitingRange.shuffled().first())

        val error = errorProbability.shuffled().first() >= errorRate

        return if (error) {
            ok()
                .syncBody(Message(from = "$name ($version)", date = now()))
                .delayElement(duration)
                .doOnNext { log.info("$name called and it responded with \"$name: $version\" ") }
        } else {
            log.error("Random error 🔥")
            ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "Random error 🔥").toMono()
        }
    }
}

data class Message(
        val from: String,
        val date: ZonedDateTime = now()
)
