#!/bin/sh
set -eou pipefail

: "${PUBLIC_HOST?"Need to set PUBLIC_HOST"}"
: "${VERSION?"Need to set VERSION"}"

SOURCE_FILES_REGEX=".*/\(\(app\|deploy\|modules\)/.*\|antora.yml\)"

# shellcheck disable=SC2038
find . -type f -regex "${SOURCE_FILES_REGEX}" \
  | xargs sed "s;Λ{PUBLIC_HOST};${PUBLIC_HOST};g" -i

# shellcheck disable=SC2038
find . -type f -regex "${SOURCE_FILES_REGEX}" \
  | xargs sed "s;Λ{VERSION};${VERSION};g" -i
