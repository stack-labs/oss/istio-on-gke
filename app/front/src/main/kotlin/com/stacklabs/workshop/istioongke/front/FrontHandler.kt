package com.stacklabs.workshop.istioongke.front

import org.slf4j.LoggerFactory
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.router
import reactor.core.publisher.Mono
import java.time.Duration.ofMillis
import java.time.ZonedDateTime.now

@Configuration
@EnableConfigurationProperties(FrontProperties::class)
@Import(FrontHandler::class, MiddlewareService::class)
class ApplicationConfiguration {
    @Bean
    fun routes(front: FrontHandler) = router {
        GET("/", front::serve)
    }
}

class FrontHandler(
        private val middleware: MiddlewareService,
        prop: FrontProperties
) {

    private val log = LoggerFactory.getLogger(FrontHandler::class.java)

    private val name = prop.name
    private val version: String = prop.version
    private val requestWaitingRange = (0..prop.maxLatency).map(Int::toLong)

    @SuppressWarnings("")
    fun serve(@Suppress("UNUSED_PARAMETER") r: ServerRequest): Mono<ServerResponse> {

        val duration = ofMillis(requestWaitingRange.shuffled().first())

        return Mono.just(1)
                .delayElement(duration)
                .flatMap { middleware.call() }
                .delayElement(duration)
                .map { it.copy(from = "$name ($version) => ${it.from}", date = now()) }
                .doOnNext { log.info("UI service in version $version called and answered with $it") }
                .flatMap { ServerResponse.ok().bodyValue(it) }
                .doOnSubscribe { log.info("UI Service in version $version starting...") }
    }
}
