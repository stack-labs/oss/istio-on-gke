resource "random_string" "project_id" {
  count = length(var.emails)

  length = 30
  upper = false
  special = false
  number = false
}

resource "random_pet" "project_name" {
  count = length(var.emails)

  length = 3
  separator = "-"
}

resource "google_project" "projects" {
  count = length(var.emails)

  name            = random_pet.project_name[count.index].id
  project_id      = random_string.project_id[count.index].result
  folder_id       = var.folder_id
  billing_account = var.billing_account

  labels = {
    attendee = lower(replace(var.emails[count.index], "/[^a-zA-Z0-9]/" ,"-"))
  }
}