# Kubernetes in Docker (kind)

Get the cluster up and running in confined environement using `kind`.

## Installation

Follow the [offical instructions](https://github.com/kubernetes-sigs/kind#installation-and-usage).


## Create cluster with local access
### Create a kind config file
```
Λ\: $ cat > kind.config.yml <<EOF
kind: Cluster
apiVersion: kind.sigs.k8s.io/v1alpha3
nodes:
- role: control-plane
  extraPortMappings:
  - containerPort: 31380
    hostPort: 8080
EOF
```

### Create the cluster
```
Λ\: $ kind create cluster --name istio-formation --config ./kind.config.yml
```


## Install Istio manually
```
Λ\: $ curl -L https://git.io/getLatestIstio | sh -

Λ\: $ export ISTIO_FOLDER=$(ls | grep istio | tail -n 1)

Λ\: $ ls ${ISTIO_FOLDER}/install/kubernetes/helm/istio-init/files/crd*yaml | xargs -n1 -P 1 -I  @ kubectl apply -f @
Λ\: $ sleep 5
Λ\: $ kubectl apply -f ${ISTIO_FOLDER}/install/kubernetes/istio-demo.yaml
```

## Reach the application
### Deploy
```
Λ\: $ kubectl apply -f k8s-base.yml
```

**CAUTION**: if using offline, you need to import the needed docker images into kind. For example:

```
Λ\: $ kind --name istio-formation load docker-image stacklabs/istio-on-gke-database
``` 

### Reach the cluster
```
Λ\: $ curl http://localhost:8080
```

## Start and stop the cluster
```
Λ\: $ docker start istio-formation-control-plane
```
```
Λ\: $ docker stop istio-formation-control-plane
```

*CAUTION*: make sure to manually check if everything is working properly when you restart clusters.