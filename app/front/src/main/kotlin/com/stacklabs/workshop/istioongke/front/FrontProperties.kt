package com.stacklabs.workshop.istioongke.front

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import java.net.URI

@ConstructorBinding
@ConfigurationProperties("front")
class FrontProperties(
        val name: String = "front",
        val middlewareUri: URI,
        val version: String = "v0",
        val maxLatency: Int = 0
)
