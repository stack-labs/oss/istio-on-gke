#!/usr/bin/env python3
import os
import argparse
from multiprocessing.dummy import Pool as ThreadPool

import google.auth
from googleapiclient import discovery
from googleapiclient.errors import HttpError
from google.oauth2 import service_account


def configure_parser(action: argparse._SubParsersAction = None, parser_cmd: str = 'iam') -> argparse.ArgumentParser:
    parser_kwargs = dict(
        description='''
            Delete given project(s)
        '''
    )
    if action is None:
        parser = argparse.ArgumentParser(**parser_kwargs)
    else:
        parser = action.add_parser(name=parser_cmd, **parser_kwargs)

    parser.add_argument(
        dest='input_path',
        help='Path to input CSV file',
    )
    parser.add_argument(
        '--project-col',
        dest='project_col',
        help='Index of the project column',
        type=int,
        default=0
    )
    parser.add_argument(
        '--separator',
        dest='separator',
        help='Separator to use to parse the CSV. Defaults to ","',
        default=','
    )
    parser.add_argument(
        '-s', '--service-account-key',
        dest='service_account_key',
        help='Path to the GCP service account key to use',
        required=True
    )
    parser.add_argument(
        '-t', '--threads',
        dest='threads',
        help='Number of threads to use to parallelize requests. Defaults to 5',
        type=int,
        default=5
    )
    return parser


def ensure_project_deletion(
        project: str,
        credentials: google.oauth2.service_account.credentials.Credentials = None
):
    service = discovery.build('cloudresourcemanager', 'v1beta1', credentials=credentials)
    request = service.projects().delete(projectId=project)

    print(f'👷 About to delete a project called "{project}"')
    try:
        request.execute()
        print('✅ Done')
    except HttpError as err:
        if err.resp.status == 403 and 'inactive project' in err.content.decode("utf-8"):
            print(f'😅 Project "{project}" already deleted')
        else:
            raise Exception(f'unknown error: {err}')


def run(arguments: argparse.Namespace):
    with open(os.path.abspath(arguments.input_path), 'r') as stream:
        content = stream.readlines()

    gcp_credentials = service_account.Credentials.from_service_account_file(
        filename=os.path.abspath(arguments.service_account_key)
    )

    pool = ThreadPool(arguments.threads)
    projects = [raw.strip().split(arguments.separator)[arguments.project_col] for raw in content]

    def delete(project_id: str):
        try:
            ensure_project_deletion(
                project=project_id,
                credentials=gcp_credentials
            )
        except Exception as e:
            print(f'❌ Failed to delete project "{project_id}": {e}')

    pool.map(delete, projects)


if __name__ == '__main__':
    run(arguments=configure_parser().parse_args())


