package com.stacklabs.workshop.istioongke.front

import org.slf4j.LoggerFactory
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToMono
import reactor.core.publisher.Mono
import java.time.ZonedDateTime

class MiddlewareService(wcb: WebClient.Builder, prop: FrontProperties) {

    private val middlewareUri = prop.middlewareUri

    private val log = LoggerFactory.getLogger(MiddlewareService::class.java)
    private val wc = wcb.baseUrl(middlewareUri.toASCIIString()).build()

    fun call(): Mono<Message> {

        log.info("Before call to MiddlewareService at url $middlewareUri")

        return wc.get()
                .retrieve()
                .bodyToMono<Message>()
                .doOnSuccess { log.info("Call made to $middlewareUri") }
    }
}

data class Message(
        val from: String,
        val date: ZonedDateTime = ZonedDateTime.now()
)
