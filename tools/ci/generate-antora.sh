#!/bin/sh
set -eou pipefail

: "${CI_PROJECT_URL?"Need to set CI_PROJECT_URL"}"

npm install antora-site-generator-lunr

export DOCSEARCH_ENGINE=lunr
export DOCSEARCH_ENABLED=true

antora generate "$(pwd)/deploy/site.yml" \
                --to-dir "$(pwd)/dist" \
                --clean \
                --require antora-site-generator-lunr \
                --generator antora-site-generator-lunr

# Fix edit this page links
# shellcheck disable=SC2038
find "$(pwd)/dist" -type f -regex ".*\.html" \
  | xargs sed "s;${CI_PROJECT_URL}/edit/HEAD/;${CI_PROJECT_URL}/edit/master/;g" -i