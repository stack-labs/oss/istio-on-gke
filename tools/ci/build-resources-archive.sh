#!/bin/sh
set -eou pipefail

SOURCE_DIRECTORY=$(pwd)/modules/02_workshop/examples
OUTPUT_DIR=$(pwd)/modules/02_workshop/attachments
OUTPUT_ARCHIVE=${OUTPUT_DIR}/resources.tar.gz
MATCH_RESOURCES_REGEX='.*\.ya?ml'

cd "${SOURCE_DIRECTORY}"

mkdir -p "${OUTPUT_DIR}"
touch "${OUTPUT_ARCHIVE}"

# shellcheck disable=SC2038
find . -type f -regex "${MATCH_RESOURCES_REGEX}" \
  | xargs tar -cvf "${OUTPUT_ARCHIVE}"

cd -
